// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue"
import App from "./App"
import router from "./router"
import store from "./vuex/vuex"
import Print from "./common/print"
console.log(text1,"webpack dev")
//development
// import $ from 'jquery'
import "./element/index"
import http from "@/http/request"
import formCreate, { maker } from "@form-create/element-ui"
import { message } from "@/common/util"
import VJsoneditor from "v-jsoneditor"
import echart from "echarts"
import "./common/global.less" // 全局公用样式
import VueClipboard from "vue-clipboard2"
import SlideVerify from "vue-monoplasty-slide-verify"
Vue.use(SlideVerify)
import item from "./views/file/item.vue"
import scroll from "vue-seamless-scroll"
Vue.component("item", item)
Vue.component("scroll", scroll)
import VueTouch from "vue-touch"
import text from "./install.js"
Vue.use(text, { someOption: true })
Vue.use(VueTouch, { name: "v-touch" })

VueTouch.config.swipe = {
  threshold: 100 //手指左右滑动距离
}
// import mavonEditor from "mavon-editor"
// import "mavon-editor/dist/css/index.css"
// Vue.use(mavonEditor)
Vue.use(VJsoneditor)
Vue.use(VueClipboard)
// import { maker } from 'form-create'
Vue.use(formCreate)
// Vue.component("VJsoneditor", VJsoneditor)
Vue.prototype.$http = http
Vue.prototype.Print = Print
Vue.prototype.maker = maker
Vue.prototype.$echarts = echart
Vue.prototype.$message = message
console.log(http)
Vue.config.devtools = true
Vue.config.productionTip = false
Vue.config.performance = true
Vue.config.optionMergeStrategies.allUser = function (fromVal, toVal, vm) {
  return 123
}
//window.scrollTo({top:100,behavior:'smooth'}) 缓慢滑动上去
// Vue.prototype.api=api
// 将所有接口放入api对象中，然后将api对象export出去
// 避免每个涉及到数据请求的文件都引入apis.js文件
// 在main.js文件将apis.js文件引入
/* eslint-disable no-new */
new Vue({
  router,
  store,
  render: (h) => h(App)

}).$mount("#app")
