const input = {
  type: "input",
  title: "商品名称",
  field: "goods_name",
  value: "iphone 7",
  col: {
    span: 12,
    labelWidth: 150
  },
  props: {
    type: "text"
  },
  validate: [
    { required: true, message: "请输入goods_name", trigger: "blur" }
  ]
}

const InputNumber = {
  type: "InputNumber",
  field: "price",
  title: "价格",
  value: 1,
  props: {
    precision: 2
  }
}

const autoComplete = {
  type: "autoComplete",
  title: "自动完成",
  field: "auto",
  value: "xaboy",
  props: {
    fetchSuggestions: function () {
      // cb([
      //   { value: queryString }, { value: queryString + queryString }
      // ])
    }
  }
}

const radio = {
  type: "radio",
  title: "是否包邮",
  field: "is_postage",
  value: "0",
  options: [
    { value: "0", label: "不包邮", disabled: false },
    { value: "1", label: "包邮", disabled: true }
  ]
}

const checkbox = {
  type: "checkbox",
  title: "标签",
  field: "label",
  value: ["1", "2", "3"],
  options: [
    { value: "1", label: "好用", disabled: true },
    { value: "2", label: "方便", disabled: false },
    { value: "3", label: "实用", disabled: false },
    { value: "4", label: "有效", disabled: false }
  ]
}

const select = {
  type: "select",
  field: "cate_id",
  title: "产品分类",
  value: ["104", "105"],
  options: [
    { value: "104", label: "生态蔬菜", disabled: false },
    { value: "105", label: "新鲜水果", disabled: false }
  ],
  props: {
    multiple: true
  }
}

const Switch = {
  type: "switch",
  title: "是否上架",
  field: "is_show",
  value: "1",
  props: {
    activeValue: "1",
    inactiveValue: "0"
  }
}

const cascader = {
  type: "cascader",
  title: "所在区域",
  field: "address",
  value: ["陕西省", "西安市", "新城区"],
  props: {
    options: [{
      value: "beijing",
      label: "北京",
      children: [
        {
          value: "gugong",
          label: "故宫"
        },
        {
          value: "tiantan",
          label: "天坛"
        },
        {
          value: "wangfujing",
          label: "王府井"
        }
      ]
    }, {
      value: "jiangsu",
      label: "江苏",
      children: [
        {
          value: "nanjing",
          label: "南京",
          children: [
            {
              value: "fuzimiao",
              label: "夫子庙"
            }
          ]
        },
        {
          value: "suzhou",
          label: "苏州",
          children: [
            {
              value: "zhuozhengyuan",
              label: "拙政园"
            },
            {
              value: "shizilin",
              label: "狮子林"
            }
          ]
        }
      ]
    }]
  }
}

const DatePicker = {
  type: "DatePicker",
  field: "section_day",
  title: "活动日期",
  value: ["2018-02-20", "2021-02-15"],
  props: {
    type: "datetimerange",
    format: "yyyy-MM-dd HH:mm:ss",
    placeholder: "请选择活动日期"
  }
}

const TimePicker = {
  type: "TimePicker",
  field: "section_time",
  title: "活动时间",
  value: [],
  props: {
    isRange: true
  }
}

const ColorPicker = {
  type: "ColorPicker",
  field: "color",
  title: "颜色",
  value: "#ff7271"
}

const upload = {
  type: "upload",
  field: "pic",
  title: "轮播图",
  value: [
    "http://img1.touxiang.cn/uploads/20131030/30-075657_191.jpg",
    "http://img1.touxiang.cn/uploads/20131030/30-075657_191.jpg"
  ],
  props: {
    type: "select",
    uploadType: "image",
    action: "/upload.php",
    name: "pic",
    multiple: true,
    accept: "image",
    limit: 2,
    onSuccess: function (res, file) {
      file.url = res.data.filePath
    }
  }
}
const rate = {
  type: "rate",
  field: "rate",
  title: "推荐级别",
  value: 3.5,
  props: {
    max: 10
  },
  validate: [
    { required: true, type: "number", min: 3, message: "请大于3颗星", trigger: "change" }
  ]
}
export const arrList = [input,
  InputNumber,
  autoComplete,
  radio,
  checkbox,
  select,
  Switch,
  cascader,
  DatePicker,
  TimePicker,
  ColorPicker,
  upload,
  rate]
export default {
  input,
  InputNumber,
  autoComplete,
  radio,
  checkbox,
  select,
  Switch,
  cascader,
  DatePicker,
  TimePicker,
  ColorPicker,
  upload,
  rate
}
