import * as echarts from "echarts"
// import axios from 'axios'
// let $ = {
//   get: function (url,callback) {
//     axios.get(url).then(res=>{
//       console.log(res)
//       callback(res.data)
//     })
//   }
// }
export const data1 = function (dom) {
  var chartDom = document.getElementById(dom)
  var myChart = echarts.init(chartDom)
  var option

  var dataAxis = ["点", "击", "柱", "子", "或", "者", "两", "指", "在", "触", "屏", "上", "滑", "动", "能", "够", "自", "动", "缩", "放"]
  var data = [220, 182, 191, 234, 290, 330, 310, 123, 442, 321, 90, 149, 210, 122, 133, 334, 198, 123, 125, 220]
  var yMax = 500
  var dataShadow = []

  for (var i = 0; i < data.length; i++) {
    dataShadow.push(yMax)
  }

  option = {
    title: {
      text: "特性示例：渐变色 阴影 点击缩放", //标题
      subtext: "Feature Sample: Gradient Color, Shadow, Click Zoom" // 副标题
    },
    xAxis: { // x坐标
      data: dataAxis, // x坐标数据
      axisLabel: {
        inside: true,
        textStyle: {
          color: "#fff" // 坐标文本颜色
        }
      },
      axisTick: {
        show: false
      },
      axisLine: {
        show: false
      },
      z: 10
    },
    tooltip: { // 加上这个就是悬停 显示数据
      trigger: "item"
    },
    yAxis: {
      axisLine: {
        show: false
      },
      axisTick: {
        show: false
      },
      axisLabel: {
        textStyle: {
          color: "#999"
        }
      }
    },
    dataZoom: [
      {
        type: "inside"
      }
    ],
    series: [ // 数据图表显示
      {
        type: "bar",
        showBackground: true,
        itemStyle: {
          color: new echarts.graphic.LinearGradient(
            0, 0, 0, 1,
            [
              { offset: 0, color: "#83bff6" },
              { offset: 0.5, color: "#188df0" },
              { offset: 1, color: "#188df0" }
            ]
          )
        },
        emphasis: {
          itemStyle: {
            color: new echarts.graphic.LinearGradient(
              0, 0, 0, 1,
              [
                { offset: 0, color: "#2378f7" },
                { offset: 0.7, color: "#2378f7" },
                { offset: 1, color: "#83bff6" }
              ]
            )
          }
        },
        data: data
      }
    ]
  }

  // Enable data zoom when user click bar.
  var zoomSize = 6
  myChart.on("click", function (params) {
    console.log(dataAxis[Math.max(params.dataIndex - zoomSize / 2, 0)])
    myChart.dispatchAction({
      type: "dataZoom",
      startValue: dataAxis[Math.max(params.dataIndex - zoomSize / 2, 0)],
      endValue: dataAxis[Math.min(params.dataIndex + zoomSize / 2, data.length - 1)]
    })
  })

  option && myChart.setOption(option)
}
export const data2 = function (dom) {
  var chartDom = document.getElementById(dom)
  var myChart = echarts.init(chartDom)
  var option

  option = {
    xAxis: {
      type: "category",
      data: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
    },
    yAxis: {
      type: "value"
    },
    series: [{
      data: [120, 200, 150, 80, 70, 110, 130],
      type: "bar",
      showBackground: true,
      backgroundStyle: {
        color: "rgba(180, 180, 180, 0.2)"
      }
    }]
  }

  option && myChart.setOption(option)
}
export const data3 = function (dom) {
  var chartDom = document.getElementById(dom)
  var myChart = echarts.init(chartDom)
  var option

  option = {
    title: {
      text: "某站点用户访问来源",
      subtext: "纯属虚构",
      left: "center"
    },
    tooltip: {
      trigger: "item"
    },
    legend: {
      orient: "vertical",
      left: "left"
    },
    series: [
      {
        name: "访问来源",
        type: "pie",
        radius: "50%",
        data: [
          { value: 1048, name: "搜索引擎" },
          { value: 735, name: "直接访问" },
          { value: 580, name: "邮件营销" },
          { value: 484, name: "联盟广告" },
          { value: 300, name: "视频广告" }
        ],
        emphasis: {
          itemStyle: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: "rgba(0, 0, 0, 0.5)"
          }
        }
      }
    ]
  }

  option && myChart.setOption(option)
}
export const data4 = function (dom) {
  var chartDom = document.getElementById(dom)
  var myChart = echarts.init(chartDom)
  var option

  option = {
    xAxis: {},
    yAxis: {},
    series: [{
      symbolSize: 20,
      data: [
        [10.0, 8.04],
        [8.07, 6.95],
        [13.0, 7.58],
        [9.05, 8.81],
        [11.0, 8.33],
        [14.0, 7.66],
        [13.4, 6.81],
        [10.0, 6.33],
        [14.0, 8.96],
        [12.5, 6.82],
        [9.15, 7.20],
        [11.5, 7.20],
        [3.03, 4.23],
        [12.2, 7.83],
        [2.02, 4.47],
        [1.05, 3.33],
        [4.05, 4.96],
        [6.03, 7.24],
        [12.0, 6.26],
        [12.0, 8.84],
        [7.08, 5.82],
        [5.02, 5.68]
      ],
      type: "scatter"
    }]
  }

  option && myChart.setOption(option)
}
export const data5 = function (dom) {
  var chartDom = document.getElementById(dom)
  var myChart = echarts.init(chartDom)
  var option

  var data = [{
    name: "Grandpa",
    children: [{
      name: "Uncle Leo",
      value: 15,
      children: [{
        name: "Cousin Jack",
        value: 2
      }, {
        name: "Cousin Mary",
        value: 5,
        children: [{
          name: "Jackson",
          value: 2
        }]
      }, {
        name: "Cousin Ben",
        value: 4
      }]
    }, {
      name: "Father",
      value: 10,
      children: [{
        name: "Me",
        value: 5
      }, {
        name: "Brother Peter",
        value: 1
      }]
    }]
  }, {
    name: "Nancy",
    children: [{
      name: "Uncle Nike",
      children: [{
        name: "Cousin Betty",
        value: 1
      }, {
        name: "Cousin Jenny",
        value: 2
      }]
    }]
  }]

  option = {
    series: {
      type: "sunburst",
      // emphasis: {
      //     focus: 'ancestor'
      // },
      data: data,
      radius: [0, "90%"],
      label: {
        rotate: "radial"
      }
    }
  }

  option && myChart.setOption(option)
}
export const data6 = function (dom) {
  var ROOT_PATH = "https://cdn.jsdelivr.net/gh/apache/echarts-website@asf-site/examples"

  var chartDom = document.getElementById(dom)
  var myChart = echarts.init(chartDom)
  var option

  var indices = {
    name: 0,
    group: 1,
    id: 16
  }
  var schema = [
    { name: "name", index: 0 },
    { name: "group", index: 1 },
    { name: "protein", index: 2 },
    { name: "calcium", index: 3 },
    { name: "sodium", index: 4 },
    { name: "fiber", index: 5 },
    { name: "vitaminc", index: 6 },
    { name: "potassium", index: 7 },
    { name: "carbohydrate", index: 8 },
    { name: "sugars", index: 9 },
    { name: "fat", index: 10 },
    { name: "water", index: 11 },
    { name: "calories", index: 12 },
    { name: "saturated", index: 13 },
    { name: "monounsat", index: 14 },
    { name: "polyunsat", index: 15 },
    { name: "id", index: 16 }
  ]

  var groupCategories = []
  var groupColors = []

  $.get(ROOT_PATH + "/data/asset/data/nutrients.json", function (data) {
    normalizeData(data)

    myChart.setOption(option = getOption(data))
  })


  function normalizeData(originData) {
    var groupMap = {}
    originData.forEach(function (row) {
      var groupName = row[indices.group]
      if (!groupMap.hasOwnProperty(groupName)) {
        groupMap[groupName] = 1
      }
    })

    originData.forEach(function (row) {
      row.forEach(function (item, index) {
        if (index !== indices.name
          && index !== indices.group
          && index !== indices.id
        ) {
          // Convert null to zero, as all of them under unit "g".
          row[index] = parseFloat(item) || 0
        }
      })
    })

    for (var groupName in groupMap) {
      if (groupMap.hasOwnProperty(groupName)) {
        groupCategories.push(groupName)
      }
    }
    var hStep = Math.round(300 / (groupCategories.length - 1))
    for (var i = 0; i < groupCategories.length; i++) {
      groupColors.push(echarts.color.modifyHSL("#5A94DF", hStep * i))
    }
  }

  function getOption(data) {
    var lineStyle = {
      normal: {
        width: 0.5,
        opacity: 0.05
      }
    }

    return {
      backgroundColor: "#333",
      tooltip: {
        padding: 10,
        backgroundColor: "#222",
        borderColor: "#777",
        borderWidth: 1
      },
      title: [
        {
          text: "Groups",
          top: 0,
          left: 0,
          textStyle: {
            color: "#fff"
          }
        }
      ],
      visualMap: {
        show: true,
        type: "piecewise",
        categories: groupCategories,
        dimension: indices.group,
        inRange: {
          color: groupColors //['#d94e5d','#eac736','#50a3ba']
        },
        outOfRange: {
          color: ["#ccc"] //['#d94e5d','#eac736','#50a3ba']
        },
        top: 20,
        textStyle: {
          color: "#fff"
        },
        realtime: false
      },
      parallelAxis: [
        { dim: 16, name: schema[16].name, scale: true, nameLocation: "end" },
        { dim: 2, name: schema[2].name, nameLocation: "end" },
        { dim: 4, name: schema[4].name, nameLocation: "end" },
        { dim: 3, name: schema[3].name, nameLocation: "end" },
        { dim: 5, name: schema[5].name, nameLocation: "end" },
        { dim: 6, name: schema[6].name, nameLocation: "end" },
        { dim: 7, name: schema[7].name, nameLocation: "end" },
        { dim: 8, name: schema[8].name, nameLocation: "end" },
        { dim: 9, name: schema[9].name, nameLocation: "end" },
        { dim: 10, name: schema[10].name, nameLocation: "end" },
        { dim: 11, name: schema[11].name, nameLocation: "end" },
        { dim: 12, name: schema[12].name, nameLocation: "end" },
        { dim: 13, name: schema[13].name, nameLocation: "end" },
        { dim: 14, name: schema[14].name, nameLocation: "end" },
        { dim: 15, name: schema[15].name, nameLocation: "end" }
      ],
      parallel: {
        left: 280,
        top: 20,
        // top: 150,
        // height: 300,
        width: 400,
        layout: "vertical",
        parallelAxisDefault: {
          type: "value",
          name: "nutrients",
          nameLocation: "end",
          nameGap: 20,
          nameTextStyle: {
            color: "#fff",
            fontSize: 14
          },
          axisLine: {
            lineStyle: {
              color: "#aaa"
            }
          },
          axisTick: {
            lineStyle: {
              color: "#777"
            }
          },
          splitLine: {
            show: false
          },
          axisLabel: {
            color: "#fff"
          },
          realtime: false
        }
      },
      animation: false,
      series: [
        {
          name: "nutrients",
          type: "parallel",
          lineStyle: lineStyle,
          inactiveOpacity: 0,
          activeOpacity: 0.01,
          progressive: 500,
          smooth: true,
          data: data
        }
      ]
    }
  }

  option && myChart.setOption(option)
}
export const data7 = function (dom) {
  var chartDom = document.getElementById(dom)
  var myChart = echarts.init(chartDom)
  var option

  option = {
    series: [{
      type: "gauge",
      axisLine: {
        lineStyle: {
          width: 30,
          color: [
            [0.3, "#67e0e3"],
            [0.7, "#37a2da"],
            [1, "#fd666d"]
          ]
        }
      },
      pointer: {
        itemStyle: {
          color: "auto"
        }
      },
      axisTick: {
        distance: -30,
        length: 8,
        lineStyle: {
          color: "#fff",
          width: 2
        }
      },
      splitLine: {
        distance: -30,
        length: 30,
        lineStyle: {
          color: "#fff",
          width: 4
        }
      },
      axisLabel: {
        color: "auto",
        distance: 40,
        fontSize: 20
      },
      detail: {
        valueAnimation: true,
        formatter: "{value} km/h",
        color: "auto"
      },
      data: [{
        value: 70
      }]
    }]
  }

  setInterval(function () {
    option.series[0].data[0].value = (Math.random() * 100).toFixed(2) - 0
    myChart.setOption(option, true)
  }, 2000)

  option && myChart.setOption(option)
}
export const data8 = function (dom) {
  var app = {}

  var chartDom = document.getElementById(dom)
  var myChart = echarts.init(chartDom)
  var option

  option = {
    legend: {},
    tooltip: {},
    dataset: {
      source: [
        ["product", "2015", "2016", "2017"],
        ["Matcha Latte", 43.3, 85.8, 93.7],
        ["Milk Tea", 83.1, 73.4, 55.1],
        ["Cheese Cocoa", 86.4, 65.2, 82.5],
        ["Walnut Brownie", 72.4, 53.9, 39.1]
      ]
    },
    xAxis: { type: "category" },
    yAxis: {},
    // Declare several bar series, each will be mapped
    // to a column of dataset.source by default.
    series: [
      { type: "bar" },
      { type: "bar" },
      { type: "bar" }
    ]
  }

  option && myChart.setOption(option)
}