import timea from './time';
import dinput from './input';
console.log(dinput, '121');
import dselect from './select';
import dradio from './redio';
import online from '../online/index';
import bind from './bind';
import scrollTop from './a';
// import {mapGetters,mapActions} from "vuex"
import qrcode from '@/component/Qrcode/index';
import xlsx from '@/component/excel/index';
import slide from '@/component/slide/index';
import model from './model';
import rend from './rend';
import btn from './but.vue';
import xlsx1 from '@/component/newExcel/index';
import { mapState, createNamespacedHelpers } from 'vuex';
import { Flex } from '@/style/index';
let { mapMutations: mamut } = createNamespacedHelpers('ma');
import _ from 'loadsh';
// import axios from "axios";
export default {
    mounted() {
        this.changeNum();
        console.log(this.num);
        var wc = new Js2WordCloud(document.getElementById('container'));

        wc.setOption({
            tooltip: {
                show: true
            },
            fontSizeFactor: 0.1, // 当词云值相差太大，可设置此值进字体行大小微调，默认0.1

            maxFontSize: 60, // 最大fontSize，用来控制weightFactor，默认60

            minFontSize: 12, // 最小fontSize，用来控制weightFactor，默认12

            tooltip: {
                show: true, // 默认：false

                backgroundColor: 'rgba(0, 0, 0, 0.701961)', // 默认：'rgba(0, 0, 0, 0.701961)'

                formatter: function (item) {
                    // 数据格式化函数，item为list的一项
                }
            },

            noDataLoadingOption: {
                // 无数据提示。

                backgroundColor: '#eee',

                text: '暂无数据',

                textStyle: {
                    color: '#888',

                    fontSize: 14
                }
            },
            list: [
                ['谈笑风生', 80],
                ['谈笑风生', 80],
                ['谈笑风生', 70],
                ['谈笑风生', 70],
                ['谈笑风生', 60],
                ['谈笑风生', 60]
            ],

            color: '#15a4fa'
        });

        // let form = new FormData();
        // let arr = [1, 2, 3];
        // arr.map((res) => {
        //   form.append("files", new File([new Blob([res])], res + ""));
        // });
        // axios({
        //   url: "/a",
        //   method: "post",
        //   data: form,
        // });
        document.documentElement.style.setProperty('--animate-duration', '2s');
    },
    created() {
        this.fndou = _.debounce(this.cli, 500);
        this.jieliu = _.throttle(this.cli, 1500);
    },
    methods: {
        ...mamut(['changeNum']),
        captureVideo(w) {
            if (this.videoUrl == '') {
                this.$message.warning('请先输入视频地址');
                return;
            }
            let canvas = document.createElement('canvas');
            let ctx = canvas.getContext('2d');
            let video = document.getElementById('video');
            canvas.width = video.videoWidth;
            canvas.height = video.videoHeight;
            let img = document.createElement('img');
            ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
            if (w == 1) return;
            this.imgList.push(canvas.toDataURL('image/png'));
        },
        clearImg() {
            this.$confirm('确认清空已截图片?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            })
                .then(() => {
                    this.imgList = [];
                    this.$message({
                        type: 'success',
                        message: '已清空!'
                    });
                })
                .catch(() => {
                    this.$message({
                        type: 'info',
                        message: '已删除'
                    });
                });
        },
        colorChange: function (val) {
            this.style['--green'] = val;
        },
        to() {
            this.$router.push({
                name: 'text',
                params: {
                    info: {
                        a: 1,
                        v: true
                    }
                }
            });
        },
        show() {
            console.log('123', 'parent123');
        },
        openbig() {
            let doc = document.documentElement;
            this.big = !this.big;

            if (this.big) {
                if (document.exitFullScreen) {
                    document.exitFullScreen();
                } else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if (document.webkitExitFullscreen) {
                    document.webkitExitFullscreen();
                }
            } else {
                doc.requestFullscreen();
            }
        },
        cli() {
            console.log(123);
        },
        inp(value) {
            this.str = value;
        },
        cha(value) {
            this.value = value;
        },
        rad(value) {
            this.radio = value;
        },
        beforeEnter: function () {
            // ...
        },
        // 当与 CSS 结合使用时
        // 回调函数 done 是可选的
        enter: function (el, done) {
            console.log(el, 'enter');
            // ...
            done();
        },
        afterEnter: function (el) {
            console.log(el, 'afterEnter');
            // ...
        },
        enterCancelled: function (el) {
            console.log(el, 'enterCancelled');
            // ...
        },

        // --------
        // 离开时
        // --------

        beforeLeave: function (el) {
            console.log(el, 'beforeLeave');
            // ...
        },
        // 当与 CSS 结合使用时
        // 回调函数 done 是可选的
        leave: function (el, done) {
            // ...
            console.log(el, 'eave');
            done();
        },
        afterLeave: function (el) {
            // ...
            console.log(el, 'afterLeave');
        },
        // leaveCancelled 只用于 v-show 中
        leaveCancelled: function (el) {
            // ...
            console.log(el, 'leaveCancelled');
        }
    },
    filters: {
        str: function (boolean) {
            return boolean === true ? '放大' : '缩小';
        }
    },
    data() {
        return {
            style: {
                '--green': 'red'
            },
            videoUrl: '/static/movie.mp4', // /static/movie.mp4 http://localhost:4000/video
            item: '',
            imgList: [],
            fndou: null,
            jieliu: null,
            cls: ['a'],
            big: true,
            color: 'red',
            str: '123',
            value: '1',
            radio: false,
            foo: 123,
            val: 12,
            show: true,
            mode: 12,
            datalist: [
                {
                    id: 1,
                    value: 0
                },
                {
                    id: 2,
                    value: 0
                },
                {
                    id: 3,
                    value: 0
                }
            ]
        };
    },
    provide: {
        pro: function () {
            return { d: 1, c: 2 };
        }
    },
    components: {
        btn,
        dinput,
        dselect,
        dradio,
        Flex,
        online,
        bind,
        xlsx1,
        qrcode,
        xlsx,
        slide,
        scrollTop,
        model,
        rend,
        timea
    },
    computed: {
        ...mapState('ma', { num: (state) => state.num })
    }
};
