// let option = {
//   color: ['blue','green','yellow','red'], // 改变标题颜色 与线条颜色 统一
//   legend: {
//       orient: 'vertical', // 标题默认水平 垂直
//       left: 'right', // 标题定位 居中 左 右
//       top:50, // 定位 的 宽高
//       textStyle: {  // 字体的颜色
//           color: 'white'
//       },

//       show: true, // 是否显示标题
//       data: [
//         {name:'自住',icon:'circle',},
//         {name:'出租',icon:'circle'},
//         {name:'空置',icon:'circle'},
//         {name:'其他',icon:'circle'}
//       ] // 标题内容
//   },
//   title: [
//     {
//       text: '房屋性质类型',
//       textStyle:{
//         color: 'yellow'
//       }
//     }
//   ],
//   label: {
//         show: false,
//         fontSize: 12,
//         fontWeight: "bold",
//         position: 'center',
//         formatter: "{d}%\n {b} ",
//         // textStyle: {
//         //   fontSize: 15,
//         //   color: 'white'
//         // }
//   },
//   // emphasis: {
//   //   label: {
//   //       show: true,
//   //       fontSize: '12',
//   //       fontWeight: 'bold',
//   //       color:'yellow'
//   //   }
//   // },
//   toolbox: {
//     show: false
//   },
//   tooltip: {
//     // show: false,
//     trigger: "item",
//     textStyle: {
//       fontSize: 12,
//       color: "yellow" //设置文字颜色
//     },
//     formatter: "{a} <br/>{b}: {c} ({d}%)"
//   },
//   series: [
//     {
//       name: "房屋类型",
//       type: "pie",
//       radius: [40, 50],
//       center: ["50%", "50%"],
//       avoidLabelOverlap: false,
//       // itemStyle: {
//       //   borderRadius: 0
//       // },
//       labelLine: {
//         show: false
//       },
//       emphasis: {
//           label: {
//               show: true,
//               fontSize: '12',
//               fontWeight: 'bold',
//               color: 'white'
//           }
//       },
//       label: {
//         show: false,
//         fontSize: 12,
//         fontWeight: "bold",
//         position: 'center',
//         formatter: "{d}%\n {b} ",
//         // textStyle: {
//         //   fontSize: 15,
//         //   color: 'white'
//         // }
//       },
//       data: [
//           {
//             value: 1048,
//             name: '自住',


//           },
//           {
//             value: 735,
//             name: '出租',


//           },
//           {
//             value: 580,
//             name: '空置',


//           },
//           {
//             value: 484,
//             name: '其他',


//           },
//           // {
//           //   value: 300,
//           //   name: '视频广告',
//           //   labelLine:{show:false},label:{show:false},

//           // }
//       ]
//     }
//   ]
// };