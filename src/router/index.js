import Vue from 'vue';
import Router from 'vue-router';
import pdfIndex from '../component/pdf/index.vue';
import index from '../views/index.vue';
import copyIndex from '../views/copy/index.vue';
import yzmIndex from '../views/yzm/index.vue';
import uploadIndex from '../views/upload/index.vue';
import noFound from '../views/noFound/index.vue';
import login from '../component/login/login.vue';
import uTable from '../views/table/index.vue';
import echart from '../views/echarts/index.vue';
import upload from '../views/file/index.vue';
import fromSub from '../views/formSub/index.vue';
import cropor from '../cropor/index.vue';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
Vue.use(Router);
function url(name) {
    return () => import(`../views/${name}`); // 异步组件
}
const titleIndex = url('title/index');
const draggableIndex = url('draggable/index');
const draggableList = url('draggable/list');
const formCreate = url('form/create');
const mavonEditorIndex = url('mavonEditor/index');
const vjsoneditorIndex = url('vjsoneditor/index');
const myselfFormIndex = url('myselfForm/shou');
const animateIndex = url('animate/index');
const mapIndex = url('map/index');
const mapWorld = url('map/world');
const studyIndex = url('study/index');
const list = require.context('../views', true, /.vue$/);
console.log(list.keys(), '1'); // 读取文件夹的子文件 正则过滤
const routers = new Router({
    mode: 'history', // hash history
    routes: [
        {
            path: '/',
            redirect: '/login'
        },
        {
            path: '/pdf',
            component: pdfIndex,
            meta: {
                title: 'PDF'
            }
        },
        {
            path: '/login',
            component: login
        },
        {
            path: '/request',
            component: function (resolve) {
                return require(['../views/request/index.vue'], resolve); // 异步组件
            }
        },
        {
            path: '/index',
            component: function (resolve) {
                return require(['../views/index.vue'], resolve); // 异步组件
            },
            // components: {
            //   // app: {
            //   //   render: function (h) {
            //   //     return h('div',{},'123')
            //   //     }
            //   // }
            // },
            children: [
                {
                    path: '/index',
                    component: animateIndex,
                    meta: {
                        title: '当前'
                    }
                },
                {
                    path: '/functional',
                    component: function (resolve) {
                        return require(['../views/functional/index.vue'], resolve); // 异步组件
                    },
                    meta: {
                        title: '函数组件'
                    }
                },
                {
                    path: '/liu',
                    component: titleIndex,
                    meta: {
                        title: '题目'
                    }
                },
                {
                    path: '/cropor',
                    component: cropor,
                    meta: {
                        title: '剪辑'
                    }
                },
                {
                    path: '/fromSub',
                    component: fromSub,
                    meta: {
                        title: '表单提交'
                    }
                },
                {
                    path: '/file',
                    component: upload,
                    meta: {
                        title: 'file'
                    }
                },
                {
                    path: '/echart',
                    component: echart,
                    meta: {
                        title: '数据可视化'
                    }
                },
                {
                    path: '/yzm',
                    component: yzmIndex,
                    meta: {
                        title: '验证码'
                    }
                },
                {
                    path: '/table',
                    component: uTable,
                    meta: {
                        title: '表格'
                    }
                },
                {
                    path: '/study',
                    component: studyIndex,
                    meta: {
                        title: '学习'
                    }
                },
                {
                    path: '/copy',
                    component: copyIndex,
                    meta: {
                        title: '复制'
                    }
                },

                {
                    path: '/map',
                    component: mapIndex,
                    meta: {
                        title: '地图1'
                    }
                },
                {
                    path: '/world',
                    component: mapWorld,
                    meta: {
                        title: '地图'
                    }
                },
                {
                    path: '/myselfForm',
                    component: myselfFormIndex,
                    meta: {
                        title: '自己'
                    }
                },
                {
                    path: '/draggable',
                    component: draggableIndex,
                    meta: {
                        title: '拖拽'
                    }
                },
                {
                    path: '/daggableList',
                    component: draggableList,
                    meta: {
                        title: '拖拽列表'
                    }
                },
                {
                    path: '/formCreate',
                    component: formCreate,
                    meta: {
                        title: '表单创建'
                    }
                },
                {
                    path: '/mavonEditorIndex',
                    component: mavonEditorIndex,
                    meta: {
                        title: '封装富文本'
                    }
                },
                {
                    path: '/vjsoneditorIndex',
                    component: vjsoneditorIndex,
                    meta: {
                        title: '富文本'
                    }
                },
                {
                    path: '/upload',
                    component: uploadIndex,
                    meta: {
                        title: '上传'
                    }
                }
            ]
        },
        {
            path: '/:id',
            component: noFound
        }
    ],
    linkActiveClass: 'acitve-class',
    linkExactActiveClass: 'unactive-class',
    scrollBehavior(to, from, savedPosition) {
        console.log(to, from, savedPosition);
        // return 期望滚动到哪个的位置
        if (savedPosition) {
            return savedPosition;
        } else {
            return { x: 0, y: 0 };
        }
    }
});
routers.beforeEach((to, from, next) => {
    NProgress.start();
    var token = sessionStorage.getItem('token');
    // 通过这个插入路由 来新增路由列表
    // 从而 通过 参数 来 控制 账号 权限 是否有 资格访问 页面
    // routers.addRoutes([{path:'/aa',component:noFound,children:[
    //   {
    //     path:'/bb',
    //     component: noFound
    //   }
    // ]}])
    // console.log(routers.getRoutes(),'123')
    if (to.path !== '/login' && token !== null) {
        next(); // 如果token存在跳转非登录页 允许成功
        // {
        //   path:"/:id",
        //   redirect: "index"
        // }, 404页面 token存在 不是deng 不在路由路径 一律进这页面
    } else if (to.path === '/login' && token !== null) {
        next({ path: '/index' }); // 是登录 允许进入 登录账号
    } else if (to.path !== '/login' && token === null) {
        // 没有token 输入其他页面 一律进入登录页
        next({ path: 'login' });
    } else {
        next();
    }
    next();
});
routers.afterEach((to, from) => {
    console.log(to, from, 'asdsss');
    document.title = to.meta.title;
    NProgress.done();
});
export default routers;
