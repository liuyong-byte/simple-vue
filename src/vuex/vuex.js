import Vue from "vue";
import vuex from "vuex";
// import http from '../http/request'
Vue.use(vuex);
const state = {
  // 仓库
  msg: "欢迎你 我的朋友",
  dev: process.env.NODE_ENV,
};
const mutations = {
  // 同步提交
  mutHello: function(stata, data) {
    console.log(stata, data);
    stata.msg = "123";
  },
};
const actions = {
  // 分发dispatch
  actHello: function({ commit }, data) {
    commit("mutHello", data);
  },
};
const getters = {
  // 获取属性
  hello(sta) {
    return sta.msg;
  },
};
const ma = {
  // 模块
  state: { num: 1 },
  mutations: {
    changeNum: function(sta, date) {
      console.log(date);
      sta.num++;
    },
  },
  actions: {
    actChangeNum({ commit }, date) {
      commit("changeNum", date);
    },
  },
  getters: {
    amin: function(sta) {
      return sta.num + "动画";
    },
  },
  namespaced: true,
};
const modules = {
  // 模块
  ma,
};
let store = new vuex.Store({
  state,
  mutations,
  actions,
  getters,
  modules,
});

store.subscribeAction((fn, state) => {
  // 异步
  console.log(fn, state);
});
store.subscribe((mutation, state) => {
  // 同步
  console.log(mutation); //
  console.log(state);
});
store.watch(
  (state, getters) => state.msg,
  (res) => {
    console.log("loaded", res); // 监听仓库
  }
);
export default store;
