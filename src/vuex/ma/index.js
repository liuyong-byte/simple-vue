const ma = {
  // 模块
  state: {num: 1},
  mutations: {
    changeNum: function (sta,date) {
      console.log(date)
      sta.num++
    }
  },
  actions: {
    actChangeNum({commit},date) {
      commit("changeNum",date)
    }
  },
  getters: {
    amin: function (sta) {
      return sta.num + "动画"
    }
  },
  namespaced: true
}

export default ma