// (function(func){
//   func()
// })(
// function () {
//   this.listSize = 0
//   this.pos = 0
//   this.dataStore = []
//   this.clear = clear
//   this.find = find
//   this.toString = toString
//   this.insert = insert
//   this.append = append
//   this.remove = remove
//   this.front = front
// }
// )
function List() {
  let clear = function () {
    delete this.dataStore;
    this.dataStore = [];
    this.listSize = this.pos = 0
    return this
  }
  let find = function (element) {
    for (var i = 0; i < this.dataStore.length; ++i) {
      if (this.dataStore[i] == element) {
        return i;
      } else {
        return -1;
      }
    }
  }
  let insert = function (elment, after) {
    
    return this.dataStore.splice(after,0,elment)
  }
  let append = function (element) {
    this.dataStore[this.listSize++] = element
    return this
  }
  let remove = function (element) {
    let index = this.find(element)
    this.dataStore.splice(index, 1)
    return this
  }
  let toString = function () {
    return this.dataStore
  }
  let front = function () {
    return this
  }
  let end = function () {

  }
  let prev = function () {

  }
  let next = function () {

  }
  let length = function () {
    return this.dataStore.length
  }
  let currPos = function () {

  }
  let moveTo = function () {

  }
  let getElement = function () {

  }
  let contains = function () {

  }
  this.listSize = 0
  this.pos = 0
  this.dataStore = [] // 初始化一个空数组来保存列表元素
  this.clear = clear
  this.find = find
  this.toString = toString
  this.insert = insert
  this.append = append
  this.remove = remove
  this.front = front
  this.end = end;
  this.prev = prev;
  this.next = next;
  this.length = length;
  this.currPos = currPos;
  this.moveTo = moveTo;
  this.getElement = getElement;
  this.length = length;
  this.contains = contains;
}