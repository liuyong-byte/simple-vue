var opn = require("opn") // 打开浏览器
var express = require("express") // express框架
var webpack = require("webpack") // 核心模块webpack
const chalk = require("chalk")
const ora = require("ora")
// const fs = require("fs")
// const {
//   SyncHook, // 在一个新的compilation创建之前执行
//   SyncLoopHook,
//   SyncWaterfallHook,
//   SyncBailHook, // 在代码块优化阶段开始时执行，插件可以在这个钩子里执行对代码块的优化
//   AsyncParallelBailHook,
//   AsyncSeriesBailHook,
//   AsyncSeriesWaterfallHook,
//   AsyncSeriesHook, // 在生成文件到output目录之前执行
//   AsyncParallelHook // 完成一次编译之前执行
// } = require("tapable")
var webpackConfig = require("../webpack.config")// 配置文件
var port = webpackConfig.devServer.port
var app = express()
var compiler = webpack(webpackConfig)
// const kooks = compiler.hooks
// kooks.run // 在编译器开始读取记录前执行
// kooks.compile // 在一个新的compilation创建之前执行
// let compilation = kooks.compilation //在一次compilation创建后执行插件 新的编译过程模块

// kooks.make // 完成一次编译之前执行
// kooks.emit // 在生成文件到output目录之前执行
// kooks.afterEmit // 在生成文件到output目录之后执行
// kooks.done() // 一次编译完成后执行
// console.log(kooks.done)
const FriendlyErrorsWebpackPlugin = require("friendly-errors-webpack-plugin")
app.get("/list", function (req, res) {
  res.json({
    body: [
      {
        name: 1,
        age: 2
      }
    ]
  })
})
var devMiddleware = require("webpack-dev-middleware")(compiler, {
  publicPath: webpackConfig.output.publicPath,
  stats: "errors-only" // 可选 注释 细节省略
//   quiet: true
})

var hotMiddleware = require("webpack-hot-middleware")(compiler, {
  log: false,
  heartbeat: 2000
})

app.use(require("connect-history-api-fallback")())
// 开启 支持history路由


app.use(devMiddleware) // 服务器webpack插件


app.use(hotMiddleware) // 热加载自动打包


app.use("/static", express["static"]("../static"))

var uri = "http://localhost:" + port

// var readyPromise = new Promise(resolve => {
//   _resolve = resolve
// })


devMiddleware.waitUntilValid(() => {
  const spinner = ora()
  spinner.succeed(chalk.green(`应用已启动:${webpackConfig.devServer.host}:${port}`))
  spinner.succeed(chalk.green("欢迎使用 liu-cli --liuyong"))
  webpackConfig.plugins.push(
    new FriendlyErrorsWebpackPlugin({
      compilationSuccessInfo: {
        messages: [`应用已启动:${webpackConfig.devServer.host}:${port}`],
        notes: ["欢迎使用 liu-cli --liuyong"]
      }
    })
  )
  if (process.env.NODE_ENV !== "testing") {
    opn(uri)
  }
})

var server = app.listen(port, function () {
  const spinner = ora()
  spinner.succeed(chalk.green("start dev server success"))
})

module.exports = {
  close: () => {
    server.close()
  }
}


