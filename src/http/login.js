import axios from "axios"
axios.interceptors.request.use((config)=>{
  console.log(config)
  if (config.data.judeg) {
    config.headers["content-type"] = "application/json"
    delete config.data.judeg
  } else {
    config.headers["content-type"] = "application/x-www-form-urlencoded"
    delete config.data.judeg
  }
  return config
},err=>{
  return err
})
export const login = function (url, data) {
  console.log("login")
  return axios({url: url,data: data,method: "POST"})
  /**
   * "content-type": "application/json"
   * {user: "1", pass: "1"}
   * "content-type": "application/x-www-form-urlencoded"
   * 设置这个 传一个普通对象进来 就好 变成
   * {"user":"1","pass":"1"}: 带：不正常的格式
   * 需要接收一个formdate
   */
}

export const getList = function (url,data) {
  return axios.post(url,data,{
    headers: {
      "content-type": "application/x-www-form-urlencoded"
    }
  })
}