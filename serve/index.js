var express = require('express')
var app = express();
var fs = require('fs')
var http = require('http').createServer(app);
var server = app.listen(8082)

const multiparty = require('multiparty')
const path = require('path')
const body = require('body-parser')
app.use('/public', express.static('public'));
app.use(body.json());  //body-parser 解析json格式数据
app.use(body.urlencoded({            //此项必须在 bodyParser.json 下面,为参数编码
  extended: true
}));
app.all('*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.header("Access-Control-Allow-Headers", "name,age,Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});
app.post('/login', (req, res) => {
  console.log(req.body)
  res.json({
    name: req.body.user,
    age: req.body.pass
  })
})
app.post('/a', (req, res) => {
  console.log(req.body)
  res.json({
    name: req.body.name,
    age: req.body.age
  })
})
app.get('/b', (req, res) => {
  console.log(req)
  res.json({
    name: req.query.name,
    age: req.query.age
  })
})
app.post('/upla',(req,res)=>{ 
  // 接收文件上传
  // let form = new multiparty.Form({
  //   uploadDir: './public' //指定上传的文件路径
  // });
  // form.parse(req, (err, field, files) => {
  //   let arr = files && files.file.map(x => { // 必须这样写
  //     fs.rename(x.path, './public/' + x.originalFilename, () => {
  //       console.log('改名')
  //     })
  //   })
  //   res.end(JSON.stringify({
  //     ok: 200
  //   }))
  // })

  // 接收 formdate数据
  console.log(req.body)
  let form = new multiparty.Form()
  let arr = []
  form.parse(req,(err,field,files)=>{
    
  })
  
  form.on('field', (name, value) => {
    // name:字段名
    // value:值
    console.log('数据:', name, value);
    arr.push({name:value})
  })

  //接收文件数据
  form.on('file', (name, file) => {
    console.log('文件:', name, file);
  })
  //表单解析完成
  form.on('close', () => {
    console.log('完成');
    res.end(JSON.stringify({ok:arr}))
  })
})
app.post('/send',async (req, res) => {
  function read () {
    return new Promise((resolve, rejects) => {
      fs.readFile('./public/2020.jpg',function(err, data) {
        resolve(data)
        rejects(err)
      })
    })
  }
  let data = await  read()
  res.send(data)
})
http.listen(3000, () => {
  console.log('listening on *:3000');
});



