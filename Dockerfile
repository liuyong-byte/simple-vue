FROM node:15.10.0-slim
COPY . ./demo
WORKDIR /demo
RUN npm install
CMD npm run dev
