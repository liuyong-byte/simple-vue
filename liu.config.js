let webpack = require('webpack')
module.exports = {
  externals: {
    vue: "Vue",
    vuex: "Vuex",
  },
  module: {
    rules: [
    ],
  },
  optimization: {
    // 代码分割 下面vendors就是分割代码之后(把相同的库或者文件都提出来打包) 你可能在想css文件去哪了 你注销代码分割 你可以看见css文件夹
    splitChunks: {
      chunks: "all",
    },
  },
  plugins: [
    new webpack.DefinePlugin({
      "text1": JSON.stringify('webpack dev 123')
    })
  ],
}